import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import TechForm from './TechForm';
import TechList from './TechList';
import ApptForm from './ApptForm';
import ApptList from './ApptList';
import ServiceHistory from './ServiceHistory';
import ManufacturersList from './ManufacturersList';
import ModelList from './Models';
import AutomobileList from './Automobiles';
import Nav from './Nav';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalespeopleList from './SalespeopleList';
import SalespersonForm from './SalespersonForm';
import SaleForm from './SaleForm';
import SaleList from './SaleList';
import ManufacturerForm from './ManufacturerForm';
import VehicleForm from './VehicleModelForm';
import AutomobileForm from './AutomobileForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="customers" element={<CustomerList/>}/>
              <Route path="customers/new" element={<CustomerForm />} />
            <Route path="salespeople" element={<SalespeopleList />} />
              <Route path = "salespeople/new" element={<SalespersonForm />} />
              <Route path = "sales" element={<SaleList />} />
              <Route path ="sales/new" element={<SaleForm />} />
              <Route path ="manufacturer/new" element={<ManufacturerForm />} />
              <Route path="models/new" element={<VehicleForm />} />
              <Route path="automobiles/new" element={<AutomobileForm />} />
            <Route path='tech/new' element={<TechForm/>} />
            <Route path='tech/' element={<TechList/>} />
            <Route path='appointments/new' element={<ApptForm/>}/>
            <Route path='appointments' element={<ApptList/>}/>
            <Route path='ServiceHistory/' element={<ServiceHistory/>} />
            <Route path='ManufacturersList/' element={<ManufacturersList/>} />
            <Route path='Models/' element={<ModelList/>} />
            <Route path='Automobiles/' element={<AutomobileList/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
