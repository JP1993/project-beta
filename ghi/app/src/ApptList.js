import React, {useEffect, useState} from 'react'


function ApptList(){
	const [appointments, setAppointments] = useState([])
	const [search, setSearch] = useState('')
	const [status, setStatus] = useState([])


	const handleSearch = event =>{
		setSearch(event.target.value)
	}

	const fetchData = async () => {
		const url = 'http://localhost:8080/api/appointments/'
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setAppointments(data.appointments)
		}
	}

	useEffect(() => {
	fetchData();
	}, [status])

	const cancelAppointment = async(appointment) => {
		const url = `http://localhost:8080/api/appointments/${appointment}/cancel`
		const response = await fetch(url, {method: "PUT"})
		if (response.ok){
			setStatus(status + 1)

		}
	}
	const finishAppointment = async(appointment) => {
		const url = `http://localhost:8080/api/appointments/${appointment}/finish`
		const response = await fetch(url, {method: "PUT"})
		if (response.ok){
			setStatus(status + 1)

		}
	}







    return(
		<div>
			<input type = 'text' value={search} onChange={handleSearch}></input>
			<button type ='button' onClick={fetchData}>Search by VIN</button>
         <h1>Appointments</h1>
			<table className ="table table_stripped">
				<thead>
					<tr>
						<th>VIN</th>
						<th>Customer Name</th>
						<th>Date</th>
                        <th>Technician</th>
                        <th>Reason</th>
					</tr>
				</thead>
				<tbody>
					{appointments?.map(appointment => {
						const appointmentDate = new Date(appointment.date_time)
						return (
							<tr key={appointment.id}>
								<td>{appointment.vin}</td>
								<td>{appointment.customer}</td>
								<td>{appointmentDate.toLocaleString()}</td>
								<td>{appointment.technician.id}</td>
								<td>{appointment.reason}</td>
								<td>{appointment.status}</td>
								<td><button type='submit' className='btn btn-danger' onClick={() => cancelAppointment(appointment.id)} >Cancel Appointment</button></td>
								<td><button className='btn btn-success' onClick={() => finishAppointment(appointment.id)}>Finish Appointment</button></td>

							</tr>
						)
					})}

				</tbody>
			</table>
		</div>
    )









}


export default ApptList;
