import React from 'react';
import { useEffect, useState} from 'react'

function SaleList() {
	const [sales, setSales] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/sales/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales)
    }
  }

	useEffect(() => {
    fetchData();
  }, []);

	const deleteSale = async(id) => {
		const saleIdUrl = `http://localhost:8090/api/sales/${id}`
		const response = await fetch(saleIdUrl, {method:"DELETE"});
		if (response.ok){
			setSales(sales.filter((sale)=> sale.id !==id));
		}
	}

	return (
		<div>
			<h1>Sales</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Salesperson Employeee ID</th>
						<th>Name</th>
						<th>Customer</th>
						<th>VIN</th>
						<th>Price</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					{sales?.map(sale => {
						return (
							<tr key={sale.id}>
								<td>{ sale.salesperson.employee_id }</td>
								<td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
								<td>{ sale.customer.first_name } {sale.customer.last_name} </td>
								<td>{ sale.automobile.vin }</td>
								<td>{ sale.price }</td>
								<td>
									<button className= "btn btn-danger" onClick={() => deleteSale(sale.id)}>Remove</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}

export default SaleList
