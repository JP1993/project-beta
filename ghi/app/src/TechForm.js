import React, { useState } from 'react'


function TechForm() {

  const [first_name, setFirstName] = useState('');
  const [last_name, setLastName] = useState('');
  const [employee_id, setEmployeeId] = useState('');

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleEmployeeIdChange = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  }



  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};

    data.first_name = first_name;
    data.last_name = last_name;
    data.employee_id = employee_id;

    const url = `http://localhost:8080/api/technicians/`;
    const response = await fetch(url, {method: "POST", body: JSON.stringify (data)});
    if (response.ok){
      setFirstName('');
      setLastName('');
      setEmployeeId('');
    }

  }





return(
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add Technician</h1>
          <form onSubmit={handleSubmit}  id="create-tech">
            <div className="form-floating mb-3">
              <input value={first_name} onChange={handleFirstNameChange} placeholder='First Name'  required type="text" name="first_name" id="first_name" className="form-control"/>
              <label>First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={last_name} onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="style_name" id="style_name" className="form-control"/>
              <label htmlFor="style_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={employee_id} onChange={handleEmployeeIdChange}  placeholder="Employee ID" required type="text" name="color" id="color" className="form-control"/>
              <label htmlFor="color">Employee Id</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
);


}


export default TechForm;
