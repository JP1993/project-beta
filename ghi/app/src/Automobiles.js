import React, {useEffect, useState} from 'react'



 function AutomobilesList(){
	const [autos, setAutomobiles] = useState([])
	const fetchData = async () =>{
		const url = 'http://localhost:8100/api/automobiles/'
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setAutomobiles(data.autos)
		}
	}

	useEffect(() => {
		fetchData();
	},[])


    return(
		<div>
         <h1>Automobiles</h1>
			<table className ="table table_stripped">
				<thead>
					<tr>
						<th>Vin</th>
						<th>Year</th>
						<th>Color</th>
						<th>Model</th>
						<th>Manufacturer</th>
					</tr>
				</thead>
				<tbody>
					{autos?.map(autos => {
						return(
							<tr key={autos.id}>
								<td>{autos.vin}</td>
								<td>{autos.year}</td>
								<td>{autos.color}</td>
								<td>{autos.model.name}</td>
								<td>{autos.model.manufacturer.name}</td>
							</tr>
						)
					})}

				</tbody>
			</table>
		</div>
    )









}


export default AutomobilesList;
