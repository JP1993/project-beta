import { useEffect, useState } from 'react'


function SaleForm() {
	const [autos, setAutos] = useState([]);
	const [salespeople, setSalespeople] = useState([]);
	const [customers, setCustomers] = useState([])
	const [auto, setAuto] = useState('');
	const [customer, setCustomer] = useState('');
	const [salesperson, setSalesperson] = useState('');
	const [price, setPrice] = useState('');

	const handleAutoChange =(event) => {
		const value = event.target.value;
		setAuto(value)
	}

	const handleCustomerChange = (event) => {
		const value = event.target.value;
		setCustomer(value)
	}

	const handlePriceChange = (event) => {
		const value = event.target.value;
		setPrice(value);
	}

	const handleSalespersonChange = (event) => {
		const value = event.target.value
		setSalesperson(value)
	}

	const handleSubmit = async (event) => {
		event.preventDefault();
		const data = {}
		data.automobile = auto
		data.salesperson = salesperson
		data.customer = customer
		data.price = price

		const salesUrl = "http://localhost:8090/api/sales/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers : {
				'Content-Type': 'application/json',
			}
		};

		const response = await fetch(salesUrl, fetchConfig);
		if (response.ok) {
			const newSale = await response.json();
			console.log(newSale)
			setAuto('');
			setSalesperson('');
			setCustomer('');
			setPrice('')
		}
	}

	const fetchData = async () => {

		const automobileUrl = 'http://localhost:8100/api/automobiles/';
		const autoresponse = await fetch(automobileUrl)
		if (autoresponse.ok) {
			const data = await autoresponse.json()
			setAutos(data.autos)
		}

		const customersUrl = 'http://localhost:8090/api/customers/';
		const customerresponse = await fetch(customersUrl)
		if (customerresponse.ok) {
			const data = await customerresponse.json()
			setCustomers(data.customers)
		}

		const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
		const salespeopleresponse = await fetch(salespeopleUrl)
		if (salespeopleresponse.ok) {
			const data = await salespeopleresponse.json()
			setSalespeople(data.salespeople)
		}
	}


  useEffect(() => {
    fetchData();
  }, []);


	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Record a sale</h1>
					<form onSubmit={handleSubmit} id="create-customer-form">
					<div className="mb-3">
              <select onChange={handleAutoChange} value={auto} required name="auto" id="auto" className="form-select">
                <option value="">Choose an automobile vin</option>
                {autos.map(auto => {
                  return (
                    <option key={auto.id} value ={auto.id}>
                    {auto.vin}
                    </option>
                  );
                })}
              </select>
            </div>
						<div className="mb-3">
              <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose a salesperson</option>
                {salespeople.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>
                      {salesperson.id}
                    </option>
                  );
                })}
              </select>
            </div>
						<div className="mb-3">
              <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                <option value="">Choose a customer</option>
                {customers.map(customer => {
                  return (
                    <option key={customer.id} value ={customer.id}>
                      {customer.id}
                    </option>
                  );
                })}
              </select>
            </div>
						<div className="form-floating mb-3">
							<input onChange={handlePriceChange} value={price} placeholder="price" required type="number" name="price" id="price" className="form-control"/>
							<label htmlFor="price">Price</label>
						</div>
						<button className="btn btn-primary" >Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default SaleForm
