from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json

from .models import Technician, Appointment, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties =[
        'id',
        'vin',
        'color',
        'year',
        'vip'
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'first_name',
        'last_name',
        'employee_id',
    ]

class AppointmentEncoder(ModelEncoder):
    model= Appointment
    properties = [
        'id',
        'date_time',
        'reason',
        'status',
        'vin',
        'customer',
        'technician',
    ]
    encoders = {
        'technician': TechnicianEncoder(),


    }



@require_http_methods(['GET', 'POST'])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,

        )
@require_http_methods(['GET', 'DELETE'])
def api_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            Appointment.objects.get(id=id).delete()
            return JsonResponse(
            {"Success": "Appointment Canceled"}
        )
        except Appointment.DoesNotExist:
            return JsonResponse({"Message": "Appointment does not exist"}, status=400)
    # else:
    #     content = json.loads(request.body)
    #     Appointment.objects.filter(id=id).update(**content)
    #     appointment = Appointment.objects.get(id=id)

    #     return JsonResponse(
    #         appointment,
    #         encoder=AppointmentEncoder,
    #         safe=False,
    #     )



@require_http_methods(['GET','POST'])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,

        )
@require_http_methods(['GET',"DELETE"])
def api_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        try:
            Technician.objects.get(id=id).delete()
            return JsonResponse(
                {"Cold World": "technician fired"}
            )
        except Technician.DoesNotExist:
            return JsonResponse({"Message": "Technician does not exist"}, status=400)

@require_http_methods(['PUT'])
def api_finish_appointment(request, id):
    if request.method =="PUT":
        appointment = Appointment.objects.get(id=id)
        appointment.finish()
        return JsonResponse(
            appointment,
            encoder = AppointmentEncoder,
            safe = False,
        )

@require_http_methods(['PUT'])
def api_cancel_appointment(request, id):
    if request.method =="PUT":
        appointment = Appointment.objects.get(id=id)
        appointment.cancel()
        return JsonResponse(
            appointment,
            encoder = AppointmentEncoder,
            safe = False,
        )
