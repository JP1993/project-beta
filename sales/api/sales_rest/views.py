from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, Customer, Sale, Salesperson

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
		]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
		]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
		]

class SalespeopleListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name",
		]

class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
		]

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "automobile",
        "customer",
        "salesperson",
		]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerDetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
    }

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "automobile",
        "customer",
        "salesperson",
		]
    encoders = {
        "automobile" : AutomobileVOEncoder(),
        "customer": CustomerDetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
		}

# done
@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse({"salespeople": salespeople}, encoder=SalespeopleListEncoder, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"Message": "Salespeople list not available, please try again."}, status = 400)
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse({"salesperson": salesperson}, encoder=SalespeopleListEncoder, safe=False)


# done
@require_http_methods(["GET", "DELETE"])
def api_detail_salesperson(request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse({"salesperson": salesperson}, encoder=SalespersonDetailEncoder, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"Message" : "Invalid id"}, status=400)
    else:
        try:
            count, _ = Salesperson.objects.get(id=id).delete()
            return JsonResponse({"Deleted" : count > 0})
        except Salesperson.DoesNotExist:
            return JsonResponse({"Message": "Salesperson does not exist."}, status=404)



@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse({"customers": customers}, encoder=CustomerListEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"Message": "Customer list not available, please try again."}, status=400)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse({"customer": customer}, encoder=CustomerDetailEncoder, safe=False)


# added put so i can change the 'phone_number' from int to string after
# changing the model "phone_number" from positivebigintegerfield
# to CharField but the numbers updated automatically. sweet. leaving it in so
# i can maybe use it so the table can have an update to it
@require_http_methods(["GET", "DELETE", "PUT"])
def api_detail_customer(request,id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse({"customer": customer}, encoder=CustomerDetailEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"Message": "Invalid customer id. Check again."}, status=400)
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.get(id=id).delete()
            return JsonResponse({"Deleted": count > 0})
        except Customer.DoesNotExist:
            return JsonResponse({"Message": "Unable to delete, customer does not exist"}, status=400)
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse({"Customer": customer}, encoder=CustomerDetailEncoder, safe=False)

# pass and returns empty list
@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        try:
            sales = Sale.objects.all()
            return JsonResponse({"sales": sales}, encoder=SaleListEncoder, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"Message": "List of sales does not exist, please try again"}, status=400)
    else:
        content = json.loads(request.body)
        try:
            auto_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=auto_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"Message": "VIN number does not exist, please try again."}, status=400)
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"Message": "Customer does not exist, please try again. please. we need the sale please try again oh my god just doIT WE NEED THE SALE"}, status=400)
        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"Message": "Salesperson does not exist, please try again."}, status=400)

        sales = Sale.objects.create(**content)
        return JsonResponse({"sale": sales}, encoder=SaleDetailEncoder, safe=False)

@require_http_methods(["GET", "DELETE"])
def api_detail_sale(request,id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse({"sale": sale}, encoder=SaleDetailEncoder, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"Message": "Sale does not exist, try again"}, status=400)
    else:
        try:
            count, _ = Sale.objects.get(id=id).delete()
            return JsonResponse({"Deleted": count > 0})
        except Sale.DoesNotExist:
            return JsonResponse({"Message": "Sale does not exist, please try again."}, status=400)
